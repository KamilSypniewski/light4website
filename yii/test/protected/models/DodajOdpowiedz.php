<?php
class DodajOdpowiedz extends CFormModel
{
    public $temat;
    public $tresc;
    public $SprawdzPole;
    public function rules()
    {
        return array(
            array('temat, tresc','required'),
            array('SprawdzPole','captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }
    public function attributeLabels()
    {
        return array(
            'temat'=>'Temat',
            'tresc'=>'Treść',
            'SprawdzPole'=>'Przepisz kod',
        );
    }
}





?>