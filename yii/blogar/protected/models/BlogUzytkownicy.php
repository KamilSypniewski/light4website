<?php
class BlogUzytkownicy extends CActiveRecord
{
public static function model($className=_CLASS_)
{
return parent::model($className);
}
public function tableName()
{
return 'blog_uzytkownicy';
}
public function rules()
{
return array (
array('uzytkownik_login, uzytkownik_haslo', 'required'),
array('uzytkownik_login', 'length', 'max'=>150),
array('uzytkownik_haslo', 'length', 'max'=>50),
array('uzytkownik_id, uzytkownik_login, uzytkownik_haslo', 'safe', 'on'=>'search'),
);
}
public function relations()
{
return array (
);
}
public function attribteLabels()
{
return array (
'uzytkownik_id' =>'ID',
'uzytkownik_login' =>'Login', 
'uzytkownik_haslo' =>'Has�o',

);
}
public function search()
{
$criteria=new CDbCriteria;

$criteria->compare('uzytkownik_id',$this->uzytkownik_id);
$criteria->compare('uzytkownik_login' ,$this->uzytkownik_login,true);
$criteria->compare('uzytkownik_haslo',$this->uzytkownik_haslo,true);

return new CActiveDataProvider($this, array (
'criteria' =>$criteria,
));
}
}
?>