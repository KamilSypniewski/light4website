<?php
class BlogKomentarze extends CActiveRecord
{
public static function model($className=_CLASS_)
{
return parent::model($className);
}
public function tableName()
{
return 'blog_komentarze';
}
public function rules()
{
return array (
array('komentarz_uzytkownik, komentarz_tresc', 'required'),
array('komentarz_uzytkownik', 'length', 'max'=>65),
array('komentarz_id, komentarz_wpis_id, komentarz_uzytkownik, komentarz_tresc, komentarz_data', 'safe', 'on'=>'search'),
);
}
public function relations()
{
return array (
'komentarzWpis' => array(self::BELONGS_TO, 'BlogWpisy', 'komentarz_wpisy_id')
);
}
public function attribteLabels()
{
return array (
'komentarz_id' =>'ID',
'komentarz_wpis_id' =>'Nazwa',
'komentarz_uzytkownik'=>'Urzytkownik', 
'komentarz_tresc'=>'Treść', 
'komentarz_data'=>'Data publikacji',

);
}
public function search()
{
$criteria=new CDbCriteria;

$criteria->compare('komentarz_id',$this->komentarz_id);
$criteria->compare('komentarz_wpis_id' ,$this->komentarz_wpis_id);
$criteria->compare('komentarz_uzytkownik',$this->komentarz_uzytkownik,true);
$criteria->compare('komentarz_tresc',$this->komentarz_tresc,true); 

return new CActiveDataProvider($this, array (
'criteria' =>$criteria,
));
}
}
?>