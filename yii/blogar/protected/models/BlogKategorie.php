<?php
class BlogKategorie extends CActiveRecord
{
public static function model($className=_CLASS_)
{
return parent::model($className);
}
public function tableName()
{
return 'blog_kategorie';
}
public function rules()
{
return array (
array('kategoria_nazwa', 'required'),
array('kategoria_nazwa', 'length', 'max'=>50),
array('kategoria_id, kategoria_nazwa', 'safe', 'on'=>'search'),
);
}
public function relations()
{
return array (
);
}
public function attribteLabels()
{
return array (
'kategoria_id' =>'ID',
'kategoria_nazwa' =>'Nazwa',
);
}
public function search()
{
$criteria=new CDbCriteria;

$criteria->compare('kategoria_id',$this->kategoria_id);
$criteria->compare('kategoria_nazwa' ,$this->kategoria_nazwa,true);

return new CActiveDataProvider($this, array (
'criteria' =>$criteria,
));
}

}


?>