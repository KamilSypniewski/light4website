<?php
class BlogZmianaHasla extends CActiveRecord
{
public $uzytkownik_haslo;
public $uzytkownik_nowehaslo;
public $uzytkownik_nowehaslo2;
public static function model($className=_CLASS_)
{
return parent::model($className);
}
public function tableName()
{
return 'blog_uzytkownicy';
}
public function rules()
{
return array (
array('uzytkownik_haslo, uzytkownik_nowehaslo, uzytkownik_nowehaslo2', 'required'),
);
}
public function attribteLabels()
{
return array (
'uzytkownik_haslo' =>'Stare hasło',
'uzytkownik_nowehaslo' =>'Nowe hasło', 
'uzytkownik_nowehaslo2' =>'Powtórz nowe hasło',

);
}
}
?>