<?php
class BlogWpisy extends CActiveRecord
{
public static function model($className=_CLASS_)
{
return parent::model($className);
}
public function tableName()
{
return 'blog_wpisy';
}
public function rules()
{
return array (
array('wpis_tytul, wpis_tresc, wpis_kategoria', 'required'),
array('wpis_kategoria', 'numerical',  'integerOnly'=>true),
array('wpis_tytul', 'length', 'max'=>65),
array('wpis_id, wpis_tytul, wpis_tresc, wpis_kategoria, wpis_data', 'safe', 'on'=>'search'),
);
}
public function relations()
{
return array (
'blogKomentarzes' => array(self::HAS_MANY, 'BlogKomentarze', 'komentarz_wpis_id'),
);
}
public function attribteLabels()
{
return array (
'wpis_id' =>'ID',
'wpis_tytul' =>'Tytuł', 
'wpis_tresc' =>'Treść', 
'wpis_kategoria' =>'Kategoria', 
'wpis_data' =>'Data',

);
}
public function search()
{
$criteria=new CDbCriteria;

$criteria->compare('wpis_id',$this->wpis_id);
$criteria->compare('wpis_tytul' ,$this->wpis_tytul,true);
$criteria->compare('wpis_tresc',$this->wpis_tresc,true);
$criteria->compare('wpis_kategoria',$this->wpis_kategoria);
$criteria->compare('wpis_data',$this->wpis_data,true);
return new CActiveDataProvider($this, array (
'criteria' =>$criteria,
));
}
}
?>