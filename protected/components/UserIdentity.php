<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $user = Yii::app()->userDao->userLogin($this->username);

        if(intval($user['is_active']) === 0) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        elseif(!isset($user['email'])) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        elseif($user['password'] !== WebUser::getPasswordHash($this->username, $this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        else {
            $this->setState('id', $user['id']);
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
	}
}