<?php 
class WebUser extends CWebUser
{
    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    public function checkAccess($operation, $params=array())
    {
        if (empty($this->id)) {
            // Not identified => no rights
            return false;
        }
        $roles = $this->getState("roles", array());
        
        if(!is_array($operation)) {
	        if(in_array($operation, $roles)) {
	        	return true;
	        }
        }
        else {
        	foreach($operation as $o) {
        		if(in_array($o, $roles)) {
        			return true;
        		}
        	}
        }
		
		return false;
    }
    
    public static function getPasswordHash($username, $password) {    	 
    	return md5($username . $password);
    }
}
?>