<?php

class NewsDao {

    public function __construct() {
    }

    public function init() {
    }

    public function getNewsInfo() {
        return Yii::app()->db->createCommand('
            SELECT n.id, n.name, n.description, n.is_active, n.created_at, n.updated_at,  CONCAT(u.first_name," ",u.last_name) as author
            FROM news n
              JOIN user u ON n.author_id = u.id
        ')
        ->queryAll();
    }
}