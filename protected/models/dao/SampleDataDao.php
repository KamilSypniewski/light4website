<?php

class SampleDataDao {

    public function __construct() {
    }

    public function init() {
    }

    public function insertSampleData($csv = array()) {

        $keys = array_map(function($k){ return ':d'.$k; }, array_keys($csv));
        $csv = array_combine($keys, $csv);

        $query = "INSERT INTO sample_data (id, first_name, last_name, email, country, ip_address) VALUES ";

        foreach ($csv as $key=>$value) {
            $query .='( '.$key.'0, '.$key.'1, '.$key.'2, '.$key.'3, '.$key.'4, '.$key.'5 ' . '),';
        }

        $query = substr($query, 0, -1);

        $command =   Yii::app()->db->createCommand($query);

        foreach($csv as $key => $value) {
            $command->bindValue($key.'0', $value[0], PDO::PARAM_INT);
            $command->bindValue($key.'1', $value[1], PDO::PARAM_STR);
            $command->bindValue($key.'2', $value[2], PDO::PARAM_STR);
            $command->bindValue($key.'3', $value[3], PDO::PARAM_STR);
            $command->bindValue($key.'4', $value[4], PDO::PARAM_STR);
            $command->bindValue($key.'5', $value[5], PDO::PARAM_STR);
        }

        return $command->execute();
    }

    public function getCountUserFromCountry() {
        return Yii::app()->db->createCommand('
           SELECT sd.country,COUNT(*) as count
           FROM sample_data sd
           GROUP BY sd.country
           ORDER BY sd.country ASC;
        ')
        ->queryAll();
    }

    public function deleteSampleDataAll() {
        return Yii::app()->db->createCommand('
            DELETE FROM  sample_data;
        ')
        ->execute();

    }


}