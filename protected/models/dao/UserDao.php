<?php

class UserDao {

    public function __construct() {
    }

    public function init() {
    }

    public function insertUser($user = array()) {
        return Yii::app()->db->createCommand('
					INSERT INTO user (first_name, last_name, email, is_active, password, created_at, is_men)
					VALUES (:firstName, :lastName, :email, :isActive, :password, NOW(),:isMen);

        		')

            ->bindValue(':firstName', $user['firstName'], PDO::PARAM_STR)
            ->bindValue(':lastName', $user['lastName'], PDO::PARAM_STR)
            ->bindValue(':email', $user['email'], PDO::PARAM_STR)
            ->bindValue(':isActive', $user['isActive'], PDO::PARAM_INT)
            ->bindValue(':password', $user['passwordHash'], PDO::PARAM_STR)
            ->bindValue(':isMen', $user['isMen'], PDO::PARAM_INT)
            ->execute();
    }

    public function userEmailExists($email) {
        $result = Yii::app()->db->createCommand('
					SELECT COUNT(u.id)
					FROM user u
					WHERE u.email = :email;
        		')
            ->bindValue(':email', $email, PDO::PARAM_STR)
            ->queryScalar();
        return $result > 0;
    }

    public function userLogin($email) {
        return Yii::app()->db->createCommand('
            SELECT u.id, u.email, u.password, u.is_active
            FROM user u
            WHERE u.is_active = 1
              AND u.email = :email
        ')
            ->bindValue(":email", $email,PDO::PARAM_STR)
            ->queryRow();
    }
}