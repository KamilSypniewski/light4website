<?php

class AddUserForm extends CFormModel {
	public $firstName;
    public $lastName;
    public $email;
    public $gender; // (boys  or girls)
    public $isActive;
    public $password;
    public $verifyCode;

	public function rules()
	{
		return array(
			array('firstName, lastName, email, gender, password ', 'required'),
            array('firstName, lastName', 'length', 'min'=>3, 'max'=>45),
            array('password', 'length', 'min'=>5, 'max'=>64),
            array('email', 'uniqueEmail'),
			array('email', 'email'),
            array('isActive', 'safe'),
            array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

    public function uniqueEmail() {
        if(Yii::app()->userDao->userEmailExists($this->email)) {
            $this->addError('email', Yii::t('main', 'User with the given email already exists'));
        }
    }

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
                'firstName' => Yii::t('main', 'First name'),
                'lastName' => Yii::t('main', 'Last name'),
                'email' => Yii::t('main', 'Email'),
                'gender' => Yii::t('main', 'Gender'),
                'password' => Yii::t('main', 'Password'),
                'isActive' => Yii::t('main', 'Is active'),
		);
	}
}