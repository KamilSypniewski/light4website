<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Light4website',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(

        'application.models.*',
        'application.models.form.*',
        'application.components.*',
        'application.helpers.*',
        'application.models.dao.*',
        'application.models.ar.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'go',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

	),

	// application components
	'components'=>array(

        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap',
            //'yiiCss'=>false,
            //'jqueryCss'=>false,
            //'coreCss'=>false,
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
        'userDao' => array('class' => 'application.models.dao.UserDao'),
        'newsDao' => array('class' => 'application.models.dao.NewsDao'),
        'sampleDataDao' => array('class' => 'application.models.dao.SampleDataDao'),


        // uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=light4website',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'logFile' => 'error.log',

                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'info',
                    'logFile' => 'info.log',
                ),
                array(
                    'class' => 'CWebLogRoute',
                    'levels' => 'info',
                    'enabled' => YII_DEBUG,
                ),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
    'aliases' => array(
      //  'yiiExtensions' => 'application.protected.extensions',
        'bootstrap' => 'ext.bootstrap'
    ),
	'params'=>array(
		// this is used in contact page
        'adminEmail'=>'kamilsypniewski@o2.pl',
	),
    'sourceLanguage' => 'en',
    'language' => 'pl',
);