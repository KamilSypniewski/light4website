<?php

class CsvController extends Controller {
    public function accessRules() {
        return array(
            array(
                'deny',
                'actions'=>array('Import','Chart'),
                'users' => array('?'),
                'deniedCallback' => function () {
                        throw new CHttpException(401, Yii::t('main', 'You must be logged in to view the page.'));
                    },
            ),
            array(
                'allow',
                'actions'=>array('Import','Chart'),

            ),
            array('deny'),
        );
    }

    public function actionImport() {

        if(Yii::app()->request->getIsAjaxRequest()) {
            $filename = 'upload/sample_data.csv';

            if (file_exists($filename)) {
                $transaction = Yii::app()->db->beginTransaction();

                try {
                    if (($handle = fopen($filename, "r")) !== FALSE) {
                        while (($data = fgetcsv($handle)) !== FALSE) {
                            $csv[] = $data;
                        }
                        fclose($handle);
                    }

                    array_shift($csv);
                    Yii::app()->sampleDataDao->deleteSampleDataAll();
                    Yii::app()->sampleDataDao->insertSampleData($csv);


                   $transaction->commit();
                    echo json_encode(array(
                        'success' => true,
                        'message' => Yii::t('main','Csv import completed successfully.')
                    ));
                }
                catch(Exception $e) {
                    $transaction->rollBack();
                    echo json_encode(array(
                        'success' => false,
                        'message' => Yii::t('main','An error occurred during import')
                    ));
                }
            }
            else {
                echo json_encode(array(
                    'success' => false,
                    'message' => Yii::t('main','The file does not exist')
                ));
            }
        }
        Yii::app()->end();

    }

    public function actionChart() {
        $dataProvider = Yii::app()->sampleDataDao->getCountUserFromCountry();

        $data = null;
        if(!empty($dataProvider)){
            $data = "['".Yii::t('main','Country') ." ','".Yii::t('main','Count') ."'],";

            foreach ($dataProvider as $value) {
                $data .= "['" . $value['country'] . "'," . $value['count'] . "],";
            }
            $data = substr($data, 0, -1);
        }

        $this->render("chart", array(
            'dataProvider' => $data,
        ));
    }
}