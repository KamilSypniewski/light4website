<div>
    <?php
    echo CHtml::ajaxSubmitButton( Yii::t('main','Import csv'),Yii::app()->createUrl('csv/Import'),
        array(
            'type'=>'POST',
            'dataType' =>'json',
            'success'=>'js:function(data){
console.log(data);
 alert(data["message"]);
                if(data["success"] == true){
                    location.reload();
                }
            }'
        ),array('class'=>'someCssClass',));
    ?>
    <p><?php echo Yii::t('main','The graph shows the number of people from that country.'); ?></p>

</div>
<div id="chart_div"></div>
<script>

    <?php
        if(!is_null($dataProvider)) {
            echo "
                google.load('visualization', '1', {packages: ['corechart', 'bar']});
                google.setOnLoadCallback(drawBarColors);
                ";
        }
    ?>

    function drawBarColors() {

        var data = google.visualization.arrayToDataTable([
            <?php echo $dataProvider ?>
        ]);

        var options = {
            chartArea: {width: '50%' ,height: data.getNumberOfRows() * 30},
            colors: ['#b0120a', '#ffab91'],
            hAxis: {
                minValue: 0
            },
            height: data.getNumberOfRows() * 30,
        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }

</script>