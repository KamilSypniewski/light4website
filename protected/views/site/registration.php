<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'contact-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'firstName'); ?>
        <?php echo $form->textField($model, 'firstName'); ?>
        <?php echo $form->error($model, 'firstName'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'lastName'); ?>
        <?php echo $form->textField($model, 'lastName'); ?>
        <?php echo $form->error($model, 'lastName'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'gender'); ?>
        <?php echo $form->dropDownList($model,'gender', array('1'=>Yii::t('main','Men'), '0'=>Yii::t('main','Woman'))); ?>
        <?php echo $form->error($model, 'gender'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->textField($model, 'password'); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'isActive'); ?>
        <?php echo $form->checkBox($model, 'isActive'); ?>
        <?php echo $form->error($model, 'isActive'); ?>
    </div>
    <div class="row">
    <?php echo $form->textField($model,'verifyCode'); ?>
    <br>
    <?php $this->widget('CCaptcha'); ?>
    <br>
    <?php echo $form->error($model,'verifyCode'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('main','Submit')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<p class="note"><?php echo Yii::t('main', 'Fields with * are required.') ?></p>