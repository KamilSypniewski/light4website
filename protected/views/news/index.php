<?php
/* @var $this NewsController */
/* @var $dataProvider CActiveDataProvider */
/* @var $model News */

$this->breadcrumbs=array(
	Yii::t('main', 'News'),
);

$this->menu=array(
    array('label'=>Yii::t('main', 'Create news'), 'url'=>array('create')),
    array('label'=>Yii::t('main', 'Manage news'), 'url'=>array('admin')),
);


?>

<h1><?php echo Yii::t('main','News');?> </h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(

    'dataProvider' => $dataProvider,
    'template' => "{items}{pager}",
    'enablePagination' => true,
    'columns' => array(
        array(
            'name' => 'id',
            'header' => '#',
            'htmlOptions' => array('color' =>'width: 60px'),
        ),
        'name',
        array(
            'name'=>'author_id',
            'type'=>'raw',
            'value'=>'$data->author->email',
        ),

        array(
            'name'=>'is_active',
            'type'=>'raw',
            'htmlOptions'=>array('style' => 'text-align: center;'),
            'value'=>'($data->is_active ? \'<div class="yesIcon"></div>\' : \'<div class="noIcon"></div>\')',
        ),
        'created_at',
        'updated_at',
        array(
            'class'=>'CButtonColumn',
        )
    ),
)); ?>