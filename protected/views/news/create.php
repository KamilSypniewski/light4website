<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
    Yii::t('main', 'News')=>array('index'),
    Yii::t('main', 'Create'),
);

$this->menu=array(
	array('label'=>Yii::t('main','List news'), 'url'=>array('index')),
	array('label'=>Yii::t('main','Manage news'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('main','Create news'); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>