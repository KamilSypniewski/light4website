<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'news-form',
        'enableAjaxValidation'=>true,

    )); ?>

	<p class="note"><?php echo Yii::t('main', 'Fields with * are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
        <?php echo $form->labelEx($model,'author_id'); ?>
        <?php echo $form->dropDownList($model, 'author_id', CHtml::listData(User::model()->findAll(), 'id', function($item) {
            return $item->email; }),array('class' => 'span5', 'options' => array(Yii::app()->user->id=>array('selected'=>true))));
        ?>
        <?php echo $form->error($model,'author_id'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('size'=>60,'maxlength'=>1000)); ?>
        <?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'is_active'); ?>
            <?php echo $form->dropDownList($model,'is_active', array( 1 => Yii::t('main','Yes'), 0 => Yii::t('main','No'))); ?>
        <?php echo $form->error($model,'is_active'); ?>
	</div>

    <?php if(!$model->isNewRecord): ?>
        <div class="row">
            <?php echo $form->labelEx($model,'created_at'); ?>
            <?php echo $form->textField($model, 'created_at', array('data-type' => 'datetime')); ?>
            <?php echo $form->error($model,'created_at'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'updated_at'); ?>
            <?php echo $form->textField($model, 'updated_at', array('data-type' => 'datetime')); ?>
            <?php echo $form->error($model,'updated_at'); ?>
        </div>
    <?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('main', 'Create') : Yii::t('main', 'Save')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
Yii::import('application.views.inc.datetimemaker', true);
?>