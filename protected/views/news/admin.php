<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
    Yii::t('main', 'News')=>array('index'),
    Yii::t('main','Manage'),
);

$this->menu=array(
	array('label'=>Yii::t('main','List news'), 'url'=>array('index')),
	array('label'=>Yii::t('main','Create news'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#news-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('main','Manage news') ?></h1>
<!--TODO kamil dodać tłumaczenia -->
<p> <?php echo Yii::t('main', 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'); ?></p>

<?php echo CHtml::link(Yii::t('main','Advanced search'),'#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'news-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
	'columns'=>array(
		'id',
        'name',
        array(
            'name'=>'author_id',
            'type'=>'raw',
            'filter'=>CHtml::listData(User::model()->findAll(), 'id', function($item) {
                    return $item->email;
                }),
            'value'=>'User::model()->findByPk($data->author_id)->email',
        ),

        array(
            'name'=>'is_active',
            'type'=>'raw',
            'filter'=>array(1 => Yii::t('main','Yes'), 0 => Yii::t('main','No')),
            'value'=>'($data->is_active ? \'<div class="yesIcon"></div>\' : \'<div class="noIcon"></div>\')',
        ),
		'created_at',
		'updated_at',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
