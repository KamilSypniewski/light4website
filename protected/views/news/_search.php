<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'author_id'); ?>
        <?php echo $form->dropDownList($model,'author_id', CHtml::listData(User::model()->findAll(), 'id', function($item) {
            return $item->email; }), array('empty'=>'', 'class'=>'textForm')); ?>
	</div>



	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('class'=>'textForm','maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('class'=>'textForm','maxlength'=>1000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_active'); ?>
        <?php echo $form->dropDownList($model,'is_active', array('' => '', 1 => Yii::t('main','Yes'), 0 => Yii::t('main','No'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at', array('class'=>'textForm', 'data-type' => 'datetime')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_at'); ?>
		<?php echo $form->textField($model,'updated_at', array('class'=>'textForm', 'data-type' => 'datetime')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('main','Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
Yii::import('application.views.inc.datetimemaker', true);
?>