<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
    Yii::t('main', 'News')=>array('index'),
	$model->name=>array('view','id'=>$model->name),
    Yii::t('main', 'Update'),
);

$this->menu=array(
	array('label'=>Yii::t('main', 'List news'), 'url'=>array('index')),
	array('label'=>Yii::t('main', 'Create news'), 'url'=>array('create')),
	array('label'=>Yii::t('main', 'View news'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('main', 'Manage news'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('main','Update news'). ' #'. $model->name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>