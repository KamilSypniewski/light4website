<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
    Yii::t('main', 'News')=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>Yii::t('main', 'List news'), 'url'=>array('index')),
	array('label'=>Yii::t('main', 'Create news'), 'url'=>array('create')),
	array('label'=>Yii::t('main', 'Update news'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('main', 'Delete news'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('main', 'Are you sure you want to delete this item?'))),
	array('label'=>Yii::t('main', 'Manage news'), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('main','View news'). ' #'. $model->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'label'=>$model->getAttributeLabel('author_id'),
            'type'=>'raw',
            'value'=>$model->author->last_name,
        ),

		'name',
        array(
            'label'=>$model->getAttributeLabel('description'),
            'type'=>'raw',
            'value'=>nl2br($model->description),
        ),

        array(
            'label'=>$model->getAttributeLabel('is_active'),
            'type'=>'raw',
            'value'=>($model->is_active ? Yii::t('main', 'Yes') : Yii::t('main', 'No')),
        ),

		'created_at',
		'updated_at',
	),
)); ?>
