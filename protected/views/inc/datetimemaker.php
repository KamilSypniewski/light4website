<?php
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.datetimepicker.js', CClientScript::POS_BEGIN);
$cs->registerCssFile(Yii::app()->request->baseUrl.'/css/jquery.datetimepicker.css');
$cs->registerScript(
		'date-time-picker',
		'
				$("input[data-type=\'datetime\']").datetimepicker({format: "Y-m-d H:i:s", step:15, theme:"dark", lang:"'.Yii::app()->language.'"});
				$("input[data-type=\'date\']").datetimepicker({format: "Y-m-d", theme:"dark", lang:"'.Yii::app()->language.'"});
			',
		CClientScript::POS_END);
?>
